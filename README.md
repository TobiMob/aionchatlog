# README #

### AionChatLog ###

Library zum verarbeiten der AION Chat.log Datei.

### AionDamageMeters ###

Damagermeter für Deutsche AION-Clients.
Zeigt den Verursachten schaden an.

![Untitled.png](https://bitbucket.org/repo/LpnyMe/images/1125707601-Untitled.png)

### EnableChatlog ###

Anwendung um Chat.log zu aktivieren.


### How do I get set up? ###

* Neuste Version herunterladen: [Downloads](https://bitbucket.org/TobiMob/aionchatlog/downloads) 
* Alle Dateien entpacken.
* EnableChatlog-Anwendung starten und system.cfg im AION-Ordner auswählen.
* AION starten und einloggen.
* AionDamageMeters-Anwendung starten.
* Unter dem Reiter "Menü" "Chatlog Datei suchen" auswählen und die "Chat.log" Datei im AION-Ordner auswählen.
* Fertig.
* Falls es Probleme gibt, AionDamageMeters als Administrator starten

### Vorraussetzungen ###

.Net Framework 4.0 (Ab Windows 8 vorinstalliert)