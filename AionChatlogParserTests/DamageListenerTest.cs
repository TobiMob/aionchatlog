﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AionChatlogParser.EventListeners;
using AionChatlogParser;

namespace AionChatlogParserTests
{
    [TestClass]
    public class DamageListenerTest
    {
        DamageListener listener = new DamageListener();
        EventListenerManager manager = new EventListenerManager();

        [TestInitialize]
        public void Initialize()
        {
            manager.Subscribe(listener);
        }

        [TestMethod]
        public void DelayedDamage()
        {
            manager.CheckLine("2016.07.24 20:12:27 : Hyperion erhält den Effekt 'Verzögerte Explosion', weil Niiru Großer Vulkanausbruch eingesetzt hat. ");
            manager.CheckLine("2016.07.24 20:12:27 : Hyperion erhält durch Großer Vulkanausbruch 100 Schaden. ");

            Assert.AreEqual(listener.GetEntity("Niiru").DamageDealt, 100);
        }

        [TestMethod]
        public void DelayedDamage2()
        {
            manager.CheckLine("2016.07.24 20:19:13 : Aphataschi benutzte Widerhall des Angriffs, um Kämpfer der Hyperion eine Verzögerung des Ketteneffekts zu geben. ");
            manager.CheckLine("2016.07.24 20:20:01 : Hyperion erhält durch Effekt Widerhall des Angriffs 100 Schaden. ");

            Assert.AreEqual(listener.GetEntity("Aphataschi").DamageDealt, 100);
        }

        [TestMethod]
        public void AutoAttack()
        {
            manager.CheckLine("2016.07.24 20:55:28 : Kritischer Treffer! Ihr habt Flachland-Vespine 100 kritischen Schaden zugefügt.");
            manager.CheckLine("2015.07.21 19:56:48 : Ihr habt Flachland-Vespine 300 Schaden zugefügt.");

            Assert.AreEqual(listener.GetEntity("Ihr").DamageDealt, 400);
        }

        [TestMethod]
        public void EffectDamage()
        {
            manager.CheckLine("2016.07.24 20:55:28 : Flachland-Vespine erhält den Effekt 'Zorn Erhöhen', weil Ihr Schlag der Züchtigung eingesetzt habt.");
            manager.CheckLine("2015.07.21 19:56:48 : Flachland-Vespine erhält durch Zorn Erhöhen 300 Schaden.");

            Assert.AreEqual(listener.GetEntity("Ihr").DamageDealt, 300);
        }

        [TestMethod]
        public void SkillDamage()
        {
            manager.CheckLine("2016.07.24 20:25:08 : Daeddiekuhl hat Informationsoffizier Kavalash durch Benutzung von Seelenschnitt 1.000 Schaden zugefügt.");
            manager.CheckLine("2016.07.24 20:25:08 : Kritischer Treffer!Daeddiekuhl hat Informationsoffizier Kavalash durch Benutzung von Seelenschnitt 2.000 Schaden zugefügt.");

            Assert.AreEqual(listener.GetEntity("Daeddiekuhl").DamageDealt, 3000);
        }

        [TestMethod]
        public void SkillWithDamageAndEffecte()
        {
            manager.CheckLine("2016.07.24 20:25:39 : Elite-Todesschütze der Hyperion-Verteidigung erhält 1.000 Schaden und den Effekt 'Siegelgravur', weil Daeddiekuhl Brüllen der Bestie eingesetzt hat.");
            manager.CheckLine("2016.07.24 20:25:39 : Kritischer Treffer!Elite-Todesschütze der Hyperion-Verteidigung erhält 2.000 Schaden und den Effekt 'Siegelgravur', weil Daeddiekuhl Brüllen der Bestie eingesetzt hat.");

            Assert.AreEqual(listener.GetEntity("Daeddiekuhl").DamageDealt, 3000);
        }

        [TestMethod]
        public void SkillWithEffect()
        {
            manager.CheckLine("2016.07.24 20:55:29 : Laryelle hat Bergrutsch eingesetzt und Leateries erleidet fortwährend Schaden.");
            manager.CheckLine("2016.07.24 20:55:33 : Leateries erhält durch Bergrutsch 100 Schaden.");

            manager.CheckLine("2016.07.24 20:55:29 : Kritischer Treffer!Laryelle hat Bergrutsch2 eingesetzt und Leateries erleidet fortwährend Schaden.");
            manager.CheckLine("2016.07.24 20:55:33 : Leateries erhält durch Bergrutsch2 300 Schaden.");

            Assert.AreEqual(listener.GetEntity("Laryelle").DamageDealt, 400);
        }

        [TestMethod]
        public void SkillWithEffectOnYou()
        {
            manager.CheckLine("2015.07.21 19:56:48 : Da Feuer-Obscura Flamme eingesetzt hat, erleidet Ihr fortwährend Schaden.");
            manager.CheckLine("2015.07.21 19:56:48 : Ihr erhaltet durch Flamme 100 Schaden.");

            manager.CheckLine("2015.07.21 19:56:48 : Kritischer Treffer!Da Feuer-Obscura Flamme2 eingesetzt hat, erleidet Ihr fortwährend Schaden.");
            manager.CheckLine("2015.07.21 19:56:48 : Ihr erhaltet durch Flamme2 300 Schaden.");

            Assert.AreEqual(listener.GetEntity("Feuer-Obscura").DamageDealt, 400);
        }

        [TestMethod]
        public void BleedDamage()
        {
            manager.CheckLine("2016.07.24 20:25:39 : Aphataschi erhält 1000 Blutungsschaden, nachdem Ihr Breiter Stich benutzt habt.");

            Assert.AreEqual(listener.GetEntity("Ihr").DamageDealt, 1000);
        }

        [TestMethod]
        public void TestEffectRemoveDot()
        {
            manager.CheckLine("2016.07.24 20:25:08 : Ritzenschwitzer fügt Informationsoffizier Kavalash durch Mana-Explosion 0 Schaden zu und hebt einige der magischen Verstärkungen und Schwächungen auf.");
            manager.CheckLine("2016.07.24 20:25:12 : Informationsoffizier Kavalash erhält durch Mana-Explosion 100 Schaden.");
            manager.CheckLine("2016.07.24 20:25:12 : other guy Kavalash erhält durch Mana-Explosion 500 Schaden.");
            manager.CheckLine("2016.07.24 20:25:12 : Informationsoffizier Kavalash erhält durch Mana-Explosion 100 Schaden.");

            Assert.AreEqual(listener.GetEntity("Ritzenschwitzer").DamageDealt, 200);
            Assert.AreEqual(listener.GetEntity("(unknown)").DamageDealt, 500);
        }

        [TestMethod]
        public void PetDamageByTarget()
        {
            manager.CheckLine("2016.07.24 19:17:46 : Ihr habt Heiliger Diener durch Beschwörung: Heiliger Diener zum Angriff auf Ziel1 herbeigerufen.");
            manager.CheckLine("2016.07.24 20:34:21 : Jainah hat Heiliger Diener durch Beschwörung: Heiliger Diener zum Angriff auf Ziel2 herbeigerufen.");

            manager.CheckLine("2016.07.24 19:17:48 : Heiliger Diener hat Ziel1 durch Benutzung von Heiliger Diener 100 Schaden zugefügt.");
            manager.CheckLine("2016.07.24 19:17:48 : Heiliger Diener hat Ziel2 durch Benutzung von Heiliger Diener 200 Schaden zugefügt.");

            Assert.AreEqual(listener.GetEntity("Ihr").DamageDealt, 100);
            Assert.AreEqual(listener.GetEntity("Jainah").DamageDealt, 200);
        }

        [TestMethod]
        public void PetDot()
        {
            manager.CheckLine("2016.07.24 20:04:41 : Acura hat Sandsturmfalle durch Falle: Sandsturm herbeigerufen.");
            manager.CheckLine("2016.07.24 20:04:41 : Sandsturmfalle hat Effekt Falle: Sandsturm eingesetzt und Elite-Todesschütze der Hyperion-Verteidigung erleidet fortwährend Schaden.");
            manager.CheckLine("2016.07.24 20:03:42 : Elite-Todesschütze der Hyperion-Verteidigung erhält durch Effekt Falle: Sandsturm 300 Schaden.");

            Assert.AreEqual(listener.GetEntity("Acura").DamageDealt, 300);
        }

        [TestMethod]
        public void PetWithoutTarget()
        {
            manager.CheckLine("2016.07.24 20:12:07 : Ritzenschwitzer hat Wassergeist durch Beschwörung: Wassergeist herbeigerufen. ");
            manager.CheckLine("2016.07.24 20:12:09 : Wassergeist hat Elite-Belagerungskanone der Hyperion-Verteidigung 300 Schaden zugefügt.");

            Assert.AreEqual(listener.GetEntity("Ritzenschwitzer").DamageDealt, 300);
        }

        [TestMethod]
        public void ApproxEffect()
        {
            manager.CheckLine("2016.07.24 20:04:43 : Ritzenschwitzer fügt Informationsoffizier Kavalash durch Magische Umkehr 1.000 Schaden zu und hebt einige der magischen Verstärkungen und Schwächungen auf.");
            manager.CheckLine("2016.07.24 20:04:48 : Informationsoffizier Kavalash erhält durch Magische Umkehr VII 100 Schaden.");

            Assert.AreEqual(listener.GetEntity("Ritzenschwitzer").DamageDealt, 1100);
        }
    }
}
