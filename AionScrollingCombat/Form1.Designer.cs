﻿namespace AionScrollingCombat
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_grid = new System.Windows.Forms.DataGridView();
            this.m_colFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_colTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_colSkill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_colDamage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.m_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // m_grid
            // 
            this.m_grid.AllowUserToAddRows = false;
            this.m_grid.AllowUserToDeleteRows = false;
            this.m_grid.AllowUserToOrderColumns = true;
            this.m_grid.AllowUserToResizeRows = false;
            this.m_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_colFrom,
            this.m_colTo,
            this.m_colSkill,
            this.m_colDamage});
            this.m_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_grid.Location = new System.Drawing.Point(0, 0);
            this.m_grid.Name = "m_grid";
            this.m_grid.RowHeadersVisible = false;
            this.m_grid.Size = new System.Drawing.Size(384, 298);
            this.m_grid.TabIndex = 0;
            // 
            // m_colFrom
            // 
            this.m_colFrom.HeaderText = "From";
            this.m_colFrom.Name = "m_colFrom";
            this.m_colFrom.ReadOnly = true;
            this.m_colFrom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.m_colFrom.Width = 75;
            // 
            // m_colTo
            // 
            this.m_colTo.HeaderText = "To";
            this.m_colTo.Name = "m_colTo";
            this.m_colTo.ReadOnly = true;
            this.m_colTo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.m_colTo.Width = 75;
            // 
            // m_colSkill
            // 
            this.m_colSkill.HeaderText = "Skill";
            this.m_colSkill.Name = "m_colSkill";
            this.m_colSkill.ReadOnly = true;
            this.m_colSkill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.m_colSkill.Width = 75;
            // 
            // m_colDamage
            // 
            this.m_colDamage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.m_colDamage.HeaderText = "Damage";
            this.m_colDamage.Name = "m_colDamage";
            this.m_colDamage.ReadOnly = true;
            this.m_colDamage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 298);
            this.Controls.Add(this.m_grid);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.m_grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView m_grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_colFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_colTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_colSkill;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_colDamage;
    }
}

