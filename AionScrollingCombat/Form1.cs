﻿using AionChatlogParser;
using AionChatlogParser.EventListeners;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AionScrollingCombat
{
    public partial class Form1 : Form
    {
        DamageListener m_listener = new DamageListener();
        EventListenerManager m_eventManager = new EventListenerManager();
        ConcurrentQueue<DamageArgs> m_queue = new ConcurrentQueue<DamageArgs>();

        public Form1()
        {
            InitializeComponent();

            m_listener.OnDamage += M_listener_OnDamage;

            m_eventManager.Subscribe(m_listener);
            //m_eventManager.StartReading(EventListenerManager.DefaultPath);
        }

        private void M_listener_OnDamage(DamageArgs args)
        {
            Invoke((MethodInvoker)(() => AddToGrid(args)));
        }

        private void AddToGrid(DamageArgs args)
        {
            m_grid.SuspendLayout();

            int n = m_grid.Rows.Add();

            m_grid.Rows[n].Cells[0].Value = args.Source;
            m_grid.Rows[n].Cells[1].Value = args.Target;
            m_grid.Rows[n].Cells[2].Value = args.Skill;
            m_grid.Rows[n].Cells[3].Value = args.Amount;

            int nMaxEntries = 20;

            if (m_grid.RowCount > nMaxEntries)
            {
                while (m_grid.RowCount > nMaxEntries)
                {
                    m_grid.Rows.RemoveAt(0);
                }
            }
            else
            {
                m_grid.FirstDisplayedScrollingRowIndex = m_grid.RowCount - 1;
            }

            m_grid.ResumeLayout();
        }
    }
}
