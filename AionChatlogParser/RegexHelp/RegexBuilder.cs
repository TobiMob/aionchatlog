﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AionChatlogParser.RegexHelp
{
    public static class RegexBuilder
    {
        /*
        private static readonly string C_ScrollUsedEN = $@"You have used (?<{g_BuffName}>[^.]+).";
        Durch {BuffName} wurde Euer Attribut {Attribute} erhöht.
        */

        public enum RegexParamType
        {
            Text,
            Number
        }

        public class RegexParam
        {
            public string Name { get; set; }
            public RegexParamType RegexType { get; set; }

            public string Parse()
            {
                string str = "{0}";

                switch (RegexType)
                {
                    case RegexParamType.Text:
                        //str = "(?<{0}>[^.]+)";
                        str = "(?<{0}>.+)";
                        break;
                    //case RegexParamType.Number:
                    //    break;
                    default:
                        Debug.Assert(false);
                        break;

                }

                return string.Format(str, Name);
            }

            public override string ToString()
            {
                return Name + $"({RegexType})";
            }
        }


        public static Regex BuildRegex(string regex, IList<RegexParam> parameter)
        {
            StringBuilder parsedRegex = new StringBuilder(regex);

            for (int i = 0; i < parameter.Count; i++)
            {
                parsedRegex.Replace("{" + parameter[i].Name + "}", parameter[i].Parse());
            }

            return new Regex(parsedRegex.ToString(), RegexOptions.IgnoreCase);
        }

        public static string DataFolder => Path.Combine(Application.StartupPath, "Data");

        public static List<Regex> LoadRegex(string fileName)
        {
            List<string> lLines = new List<string>();
            string strHeader;

            fileName = Path.Combine(DataFolder, fileName + ".csv");
            using (StreamReader reader = new StreamReader(fileName, Encoding.Default))
            {
                strHeader = reader.ReadLine();

                while (!reader.EndOfStream)
                {
                    lLines.Add(reader.ReadLine());
                }
            }

            IList<RegexParam> paras = ParseParameters(strHeader);
            List<Regex> lRegexs = new List<Regex>();

            foreach (string regexLine in lLines)
            {
                Regex r = BuildRegex(regexLine, paras);
                lRegexs.Add(r);
            }

            return lRegexs;
        }

        private static IList<RegexParam> ParseParameters(string strHeader)
        {
            List<RegexParam> lParams = new List<RegexParam>();

            string[] strParams = strHeader.Split(',');

            for (int i = 0; i < strParams.Length; i++)
            {
                string[] strSingle = strParams[i].Split('=');

                RegexParamType type = RegexParamType.Text;
                Enum.TryParse(strSingle[1], out type);

                lParams.Add(new RegexParam() { Name = strSingle[0], RegexType = type });
            }

            return lParams;
        }
    }
}
