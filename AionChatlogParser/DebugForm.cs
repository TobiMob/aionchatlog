﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AionChatlogParser
{
    public partial class DebugForm : Form
    {
        private EventListenerManager m_listener = null;

        public DebugForm(EventListenerManager eventListener)
        {
            InitializeComponent();
            m_listener = eventListener;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Send();
        }

        private void button1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Send();
        }

        private void Send()
        {
            m_listener.CheckLineDebug(textBox1.Text);
        }

        private void btnDmg_Click(object sender, EventArgs e)
        {
            textBox1.Text = $"{RndStr()} hat {RndStr()} durch Benutzung von {RndStr()} {Rnd()} Schaden zugefügt.";
        }

        Random rnd = new Random();

        private int Rnd()
        {
            rnd = new Random(rnd.Next());

            return rnd.Next(1000, 9000);
        }

        private string RndStr()
        {
            return Rnd().ToString("X");
        }
    }
}
