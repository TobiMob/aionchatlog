﻿using AionChatlogParser.EventListeners;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AionChatlogParser
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            DamageListener listener = new DamageListener();
            listener.OnDamage += Listener_OnDamage;
            EventListenerManager manager = new EventListenerManager();

            manager.Subscribe(listener);
            manager.StartReading(@"D:\Program Files (x86)\Gameforge\AION Free-To-Play\Chat.log");

            while (true)
            {
                Thread.Sleep(1000);
            }
            Console.ReadLine();
        }

        private static void Listener_OnDamage(DamageArgs obj)
        {
            Console.WriteLine(obj.Source + "->" + obj.Target + ": " + obj.Skill + " " + obj.Amount);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.ToString());
        }
    }
}
