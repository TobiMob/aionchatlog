﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AionChatlogParser.EventListeners
{
    public interface IEventListener
    {
        IEnumerable<EventSubscription> GetEvents();
        string DisplayName { get; }

        void Initialize(EventListenerInitializeArgs args);
    }

    public class EventListenerInitializeArgs
    {
        public EntityManager EntityManager { get; set; }
    }

}
