﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AionChatlogParser.EventListeners
{
    public enum Faction : byte
    {
        Any = 0,
        Ally = 1,
        Enemy = 2,
    }

    public class FactionData : Data, INotifyPropertyChanged
    {
        private Faction m_faction = Faction.Any;

        public Faction Faction
        {
            get { return m_faction; }
            set
            {
                m_faction = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(Faction)));
            }
        }

        public FactionData()
        {

        }
    }
}
