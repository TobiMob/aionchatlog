﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AionChatlogParser.EventListeners
{
    public class FactionListener : EventListenerBase
    {
        public override string DisplayName
        {
            get { return nameof(FactionListener); }
        }

        public override IEnumerable<EventSubscription> GetEvents()
        {
            yield return new EventSubscription(C_JoinedAlliDE, OnAlliJoined);
            yield return new EventSubscription(C_LeftAlliDE, OnAlliLeft);
            yield return new EventSubscription(C_LeftAlliDE, OnAlliLeft);
        }

        private void OnAlliJoined(LogEventArgs args)
        {
            string strName = args[g_name];

            GetData(strName).Faction = Faction.Ally;
        }

        private void OnAlliLeft(LogEventArgs args)
        {
            string strName = args[g_name];
            
            GetData(strName).Faction = Faction.Any;
        }

        private FactionData GetData(string strName)
        {
            return EntityManager.GetEntity(strName).GetData<FactionData>();
        }

        private const string g_name = "name";

        private readonly string C_JoinedAlliDE = $@"(?<{g_name}>.+) (ist|seid) der Allianz beigetreten\.";
        private readonly string C_LeftAlliDE = $@"(?<{g_name}>.+) (wurde|wurdet) aus der Allianz geworfen\.";
        private readonly string C_LeftAlliDE2 = $@"(?<{g_name}>.+) (hat|habt) die Allianz verlassen.\.";
    }
}
