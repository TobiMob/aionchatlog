﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AionChatlogParser.EventListeners
{
    public class EventSubscription
    {
        /// <summary>
        /// The Regexes which should be tested
        /// </summary>
        public IList<Regex> Regexes { get; private set; }

        /// <summary>
        /// The Delegate that sould be called, if a regex matches
        /// </summary>
        public OnEventMatchDelegate Delegate { get; private set; }

        #region Ctor
        public EventSubscription(string regEx, OnEventMatchDelegate deleg) : this(new string[] { regEx }, deleg)
        {
        }

        public EventSubscription(string[] regEx, OnEventMatchDelegate deleg)
        {
            Regexes = new Regex[regEx.Length];

            for (int i = 0; i < regEx.Length; i++)
            {
                Regexes[i] = new Regex(regEx[i], RegexOptions.Compiled);
            }

            Delegate = deleg;
        }

        public EventSubscription(IList<Regex> regEx, OnEventMatchDelegate deleg)
        {
            Regexes = regEx;
            Delegate = deleg;
        }
        #endregion
    }

    #region LogEventArg
    public class LogEventArgs
    {
        public DateTime Timestamp { get; set; }
        public Match RegExMatch { get; set; }
        public Regex RegEx { get; set; }

        /// <summary>
        /// Gibt den RegEx-Group-Wert zurueck
        /// </summary>
        public string this[string arg]
        {
            get
            {
                return RegExMatch.Groups[arg].Value;
            }
        }
    }
    #endregion

    public delegate void OnEventMatchDelegate(LogEventArgs args);
}
