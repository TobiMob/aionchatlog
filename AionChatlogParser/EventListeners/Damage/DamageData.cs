﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AionChatlogParser.EventListeners
{
    public class DamageData : Data, INotifyPropertyChanged
    {
        private float m_nTotalDamage = 0;
        private ConcurrentDictionary<string, float> dic_DamageBySkill = new ConcurrentDictionary<string, float>();

        #region Properties
        /// <summary>
        /// Total Damage dealt
        /// </summary>
        public float DamageDealt
        {
            get
            {
                return m_nTotalDamage;
            }
            private set
            {
                m_nTotalDamage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(DamageDealt)));
                //NotifyChange();
            }
        }
        #endregion

        #region NotifyChange
        /* .net 4.5 only
        private void NotifyChange([CallerMemberName] string caller = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
            }
            catch (Exception ex)
            {
            }
        }
        */
        #endregion

        #region AddDamage
        /// <summary>
        /// Register damage for the given skill
        /// </summary>
        internal void AddDamage(string strSkill, float fDmg)
        {
            dic_DamageBySkill.AddOrUpdate(strSkill,
                (str) =>
                {
                    return AddFactory(str, fDmg);
                },
                (str, oldValue) =>
                {
                    return UpdateFactory(str, oldValue, fDmg);
                });
        }

        private float AddFactory(string strSkill, float value)
        {
            return UpdateFactory(strSkill, 0, value);
        }

        private float UpdateFactory(string strSkill, float oldValue, float newValue)
        {
            DamageDealt += newValue;
            return oldValue + newValue;
        }
        #endregion

        #region ResetDamage
        /// <summary>
        /// Reset the damage for all skills to 0
        /// </summary>
        internal void ResetDamage()
        {
            //TODO: Possible race condition
            DamageDealt = 0;
            dic_DamageBySkill.Clear();

        }
        #endregion

        #region Pet
        /// <summary>
        /// True if the Entity is the pet of someone
        /// </summary>
        [Browsable(false)]
        public bool IsPet { get; private set; } = false;

        /// <summary>
        /// If entity is a pet and was not summoned at a specific target.
        /// this variable contains the name of the owner
        /// </summary>
        private string m_strPetOwner = "";

        /// <summary>
        /// If the Entity was summoned at a target, the key will contain the target, and the value the owner
        /// </summary>
        private ConcurrentDictionary<string, string> m_dicPetOwnerByTarget = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// Sets the owner, without a specific target
        /// </summary>
        /// <param name="strOwner"></param>
        public void SetOwner(string strOwner)
        {
            SetOwner(strOwner, null);
        }

        /// <summary>
        /// Sets the owner, with a specific target
        /// </summary>
        public void SetOwner(string strOwner, string strTarget)
        {
            IsPet = true;

            if (string.IsNullOrWhiteSpace(strTarget))
            {
                m_strPetOwner = strOwner;
                m_dicPetOwnerByTarget.Clear();
            }
            else
            {
                m_strPetOwner = "";
                m_dicPetOwnerByTarget[strTarget] = strOwner;
            }
        }

        /// <summary>
        /// Gets the pet owner, which summoned the pet.
        /// </summary>
        public string GetOwner(string strTarget)
        {
            string owner;

            if (string.IsNullOrWhiteSpace(m_strPetOwner))
            {
                //multiple owners, get the owner, who summoned the pet for the given target
                if (!m_dicPetOwnerByTarget.TryGetValue(strTarget, out owner))
                    owner = "(unknown pet owner)";
            }
            else
            {
                //only a single owner
                owner = m_strPetOwner;
            }

            return owner;
        }
        #endregion

        public IDictionary<string, float> GetEntries()
        {
            return dic_DamageBySkill;
        }

        public override string ToString()
        {
            return "Entity: " + Name;
        }
    }
}
