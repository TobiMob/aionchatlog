﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AionChatlogParser.EventListeners
{
    public class DamageArgs
    {
        public string Source { get; set; }
        public string Target { get; set; }
        public string Skill { get; set; }
        public float Amount { get; set; }
        public float NewTotal { get; set; }
    }

    public class DamageListener : EventListenerBase
    {
        #region Member
        public ConcurrentDictionary<string, string> m_dicEffectSource = new ConcurrentDictionary<string, string>();
        #endregion

        #region Events
        /// <summary>
        /// Called if a Entity infliced Damage
        /// </summary>
        public event Action<DamageArgs> OnDamage;

        /// <summary>
        /// Called if a new entity was added
        /// </summary>
        public event Action<DamageData> NewDamageDataAdded;
        #endregion

        #region Properties
        public override string DisplayName
        {
            get { return "DamageListener"; }
        }
        #endregion

        #region GetEvents
        public override IEnumerable<EventSubscription> GetEvents()
        {
            yield return new EventSubscription(C_InflictedDmgEN, OnDamageInflicted);
            yield return new EventSubscription(C_InflictedDmgDE, OnDamageInflicted);
            yield return new EventSubscription(C_ApplyDamageEffectEN, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE2, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE3, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE4, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE5, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE6, OnEffectApplied);
            yield return new EventSubscription(C_ApplyDamageEffectDE7, OnEffectApplied);
            yield return new EventSubscription(C_BleedDamageDE, OnEffectApplied);
            yield return new EventSubscription(C_ReceivedDmgByEffectEN, OnEffektDamage);
            yield return new EventSubscription(C_ReceivedDmgByEffectDE, OnEffektDamage);
            yield return new EventSubscription(C_ReceivedDmgByEffectDE2, OnEffektDamage);
            yield return new EventSubscription(C_SummonPetDE, OnPetSmmoned);
            yield break;
        }
        #endregion

        #region RegEx
        private const string g_Crit = "crit";
        private const string g_name = "name";
        private const string g_dmg = "damage";
        private const string g_target = "target";
        private const string g_skill = "skill";
        private const string g_pet = "pet";

        private readonly string C_InflictedDmgEN = $@"(?<{g_Crit}>Critical Hit!)?( )?(?<{g_name}>.+) inflicted (?<{g_dmg}>[^a-zA-Z]+) damage on (?:(?<{g_target}>.+) by using (?<{g_skill}>.+)\.|(?<{g_target}>.+)\.)";
        private readonly string C_InflictedDmgDE = $@"(?<{g_Crit}>Kritischer Treffer!)?( )?(?<{g_name}>.+) (habt|hat) (?<{g_target}>.+?) (durch Benutzung von (?<{g_skill}>.+) )*(?<{g_dmg}>[^a-zA-Z]+) (kritischen )?Schaden zugefügt\.";
        private readonly string C_BleedDamageDE = $@"(?<{g_name}>.+) erhält (?<{g_dmg}>[^a-zA-Z]+) Blutungsschaden, nachdem (?<{g_name}>\S+) (?<{g_skill}>.+?) benutzt habt.";
        private readonly string C_SummonPetDE = $@"(?<{g_name}>.+) (habt|hat) (?<{g_pet}>.+) durch (?<{g_skill}>.+?) (zum Angriff auf (?<{g_target}>.+) )*herbeigerufen.";
        private readonly string C_ApplyDamageEffectEN = $@"(?<{g_name}>.+) inflicted continuous damage on (?<{g_target}>.+) by using (?<{g_skill}>.+)\.";
        private readonly string C_ApplyDamageEffectDE = $@"(?<{g_Crit}>Critical Hit!)?Da (?<{g_name}>\S+) (?<{g_skill}>.+?) eingesetzt hat, erleidet (?<{g_target}>Ihr) fortwährend Schaden\.";
        private readonly string C_ApplyDamageEffectDE2 = $@"(?<{g_Crit}>Kritischer Treffer!)?(?<{g_name}>.+?) hat (Effekt )?(?<{g_skill}>.+?) eingesetzt und (?<{g_target}>.+?) erleidet fortwährend Schaden\.";
        private readonly string C_ApplyDamageEffectDE3 = $@"(?<{g_name}>.+?) fügt (?<{g_target}>.+?) durch (?<{g_skill}>.+?) fortwährend Schaden zu\.";
        private readonly string C_ApplyDamageEffectDE4 = $@"(?<{g_name}>.+?) fügt (?<{g_target}>.+?) durch (?<{g_skill}>.+?) (?<{g_dmg}>[^a-zA-Z]+) Schaden zu und hebt einige der magischen Verstärkungen und Schwächungen auf\.";
        private readonly string C_ApplyDamageEffectDE5 = $@"(?<{g_target}>.+) erhält (?<{g_dmg}>[^a-zA-Z]+) Schaden und den Effekt '(?<{g_skill}>.+)', weil (?<{g_name}>\S+) (?<g_effectSource>.+) eingesetzt (hat|habt)\.";
        private readonly string C_ApplyDamageEffectDE6 = $@"(?<target>.+?) (erhaltet|erhält) den Effekt ('|"")(?<skill>.+?)('|""), weil (?<name>.+?) (?<effectSource>.+?) eingesetzt (hat|habt)\.";
        private readonly string C_ApplyDamageEffectDE7 = $@"(?<{g_name}>.+?) benutzte (?<{g_skill}>.+?), um (?<{g_target}>.+?) eine Verzögerung des Ketteneffekts zu geben\.";
        private readonly string C_ReceivedDmgByEffectDE  = $@"(?<{g_target}>.+?) (erhält|erhaltet) durch ((Wirkung ')|(Effekt ))?(?<{g_skill}>.+?)'? (?<{g_dmg}>[^a-zA-Z]+) Schaden\.";
        private readonly string C_ReceivedDmgByEffectDE2 = $@"(?<{g_target}>.+?) (erhält|erhaltet) (?<{g_dmg}>[^a-zA-Z]+) Giftschaden durch den Einsatz von (?<{g_skill}>.+?)\.";
        private readonly string C_ReceivedDmgByEffectEN = $@"(?<{g_target}>.+) received (?<{g_dmg}>[^a-zA-Z]+) damage due to the effect of (?<{g_skill}>.+)\.";
        #endregion

        #region OnDamageInflicted
        /// <summary>
        /// Called if someone inflicted direct damage to a target.
        /// If <see cref="g_skill"/> is empty, "Autoattack" will be used
        /// </summary>
        public void OnDamageInflicted(LogEventArgs args)
        {
            bool blnCrit = args[g_Crit] != "" ? true : false;
            string strSource = args[g_name];
            string strTarget = args[g_target];
            string strSkill = args[g_skill];
            string strDmg = args[g_dmg];

            if (strSkill == "")
            {
                strSkill = "Autoattack";
            }

            RegisterDamage(strSource, strTarget, strSkill, strDmg);
        }
        #endregion

        #region OnEffectApplied
        /// <summary>
        /// Someone applied a effect to a target.
        /// <see cref="g_dmg"/> can be provided, if initial damage was inflicted
        /// </summary>
        private void OnEffectApplied(LogEventArgs args)
        {
            string strSource = args[g_name];
            string strTarget = args[g_target];
            string strSkill = args[g_skill];
            string strDmg = args[g_dmg];
            string strEffectSource = args["effectSource"];

            m_dicEffectSource[strTarget + "_" + strSkill] = strSource;

            //somtimes the dot hat a different name than the skill
            if(!string.IsNullOrWhiteSpace(strEffectSource))
                m_dicEffectSource[strTarget + "_" + strEffectSource] = strSource;

            if (!string.IsNullOrWhiteSpace(strDmg))
                RegisterDamage(strSource, strTarget, strSkill, strDmg);
        }
        #endregion

        #region OnEffektDamage
        /// <summary>
        /// Called if someone received damag by a effekt
        /// </summary>
        private void OnEffektDamage(LogEventArgs args)
        {
            string strSource;
            string strTarget = args[g_target];
            string strSkill = args[g_skill];
            string strDmg = args[g_dmg];

            //Try to find source of the effekt
            if (!m_dicEffectSource.TryGetValue(strTarget + "_" + strSkill, out strSource))
            {
                strSource = FindBySimilarSkill(strTarget, strSkill);
            }
            else
            {//found
            }

            RegisterDamage(strSource, strTarget, strSkill, strDmg);
        }
        #endregion

        #region FindBySimilarSkill
        /// <summary>
        /// Tries to find a effect source for a similar skill name
        /// </summary>
        /// <returns>returns the entity-name or (unknown)</code></code></returns>
        private string FindBySimilarSkill(string strTarget, string strSkill)
        {
            //default return
            string strSource = "(unknown)";

            string strSkillModified = strSkill;

            //if the skill ends with skill level: Magische Umkehr VII
            //remove the skill level: Magische Umkehr
            if (strSkillModified.Contains(' '))
            {
                strSkillModified = strSkillModified.Substring(0, strSkillModified.LastIndexOf(' '));
            }

            //add target name
            strSkillModified = strTarget + "_" + strSkillModified;

            //try to find something similar
            foreach (KeyValuePair<string, string> pair in m_dicEffectSource)
            {
                if (pair.Key.Contains(strSkillModified))
                {
                    //close enough
                    strSource = pair.Value;
                    break;
                }
            }

            return strSource;
        }
        #endregion

        #region RegisterDamage
        /// <summary>
        /// Increment DamageDealt for the given source
        /// </summary>
        private void RegisterDamage(string strSource, string strTarget, string strSkill, string strDmg)
        {
            DamageData dmgSource;

            float fDmg = float.Parse(strDmg.Replace(".", ""), CultureInfo.InvariantCulture);

            dmgSource = GetOrAddEntity(strSource);

            if (dmgSource.IsPet)
            {
                //Find owner for the pet
                string owner = dmgSource.GetOwner(strTarget);
                DamageData petOwner = GetOrAddEntity(owner);

                //Add Pet Name to skill
                strSkill = dmgSource.Name + ": " + strSkill;

                //assign damage to the pets owner
                dmgSource = petOwner;
            }
            else
            {//not a pet
            }

            //Increment DamageDealt
            dmgSource.AddDamage(strSkill, fDmg);

            //Report Damage to listeners
            DamageArgs args = new DamageArgs();
            args.Source = strSource;
            args.Target = strTarget;
            args.Skill = strSkill;
            args.Amount = fDmg;

            OnDamage?.Invoke(args);
        }
        #endregion

        #region OnPetSummoned
        /// <summary>
        /// Called if someone summoned a pet.
        /// <see cref="g_target"/> can be provided, if the pet has a fixed target
        /// </summary>
        /// <param name="args"></param>
        private void OnPetSmmoned(LogEventArgs args)
        {
            string strCaster = args[g_name];
            string strTarget = args[g_target];
            string strSkill = args[g_skill];
            string strPet = args[g_pet];

            DamageData e = GetOrAddEntity(strPet, true);

            //assign owner to pet
            e.SetOwner(strCaster, strTarget);
        }
        #endregion

        #region GetOrAddEntity
        /// <summary>
        /// Get or create the entity with the given name
        /// </summary>
        /// <param name="strName">Name of the entity</param>
        /// <param name="blnSupressAddedEvent">true, if <see cref="NewDamageDataAdded"/> should be supressed</param>
        /// <returns></returns>
        private DamageData GetOrAddEntity(string strName, bool blnSupressAddedEvent = false)
        {
            DamageData playerEntry;

            if (EntityManager.GetEntity(strName).GetData(out playerEntry))
            {
                if (!blnSupressAddedEvent)
                    NewDamageDataAdded?.Invoke(playerEntry);
            }

            return playerEntry;
        }
        #endregion

        #region GetEntity
        /// <summary>
        /// Try to find the given entity.
        /// If no Entity was found, a new DamageData will be created
        /// </summary>
        public DamageData GetEntity(string strName)
        {
            return EntityManager.GetEntity(strName).GetData<DamageData>();
        }
        #endregion

        #region ResetDamage
        /// <summary>
        /// Reset Damage for all Entities
        /// </summary>
        public void ResetDamage()
        {
            foreach (Entity e in EntityManager.GetAllEntities())
            {
                e.GetData<DamageData>().ResetDamage();
            }
        }
        #endregion
    }
}
