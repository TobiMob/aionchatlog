﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AionChatlogParser.EventListeners
{
    public class Entity : INotifyPropertyChanged
    {
        public ConcurrentDictionary<Type, Data> m_dicData = new ConcurrentDictionary<Type, Data>();

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name { get; private set; }

        public Entity(string name)
        {
            this.Name = name;
        }


        public T GetData<T>() where T : Data, new()
        {
            T data;

            GetData(out data);

            return data;
        }

        public T GetData<T>(Action<T> onDataCreated) where T : Data, new()
        {
            T data;

            if (GetData(out data))
            {
                onDataCreated(data);
            }

            return data;
        }

        public bool GetData<T>(out T dataOut) where T : Data, new()
        {
            Data data;
            bool blnCreated = false;

            if (!m_dicData.TryGetValue(typeof(T), out data))
            {
                data = new T();
                if (!m_dicData.TryAdd(typeof(T), data))
                {
                    m_dicData.TryGetValue(typeof(T), out data);
                }
                else
                {
                    //created
                    blnCreated = true;
                    data.SetParent(this);
                    data.PropertyChanged += PropertyChanged;
                }
            }
            
            dataOut = (T)data;
            return blnCreated;
        }
    }

    public abstract class Data
    {
        public virtual string Name
        {
            get
            {
                return Parent?.Name ?? "";
            }
        }

        public Entity Parent
        {
            get; private set;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            PropertyChanged?.Invoke(sender, args);
        }

        public void SetParent(Entity e)
        {
            Parent = e;
            e.PropertyChanged += PropertyChanged;
        }
    }
}
