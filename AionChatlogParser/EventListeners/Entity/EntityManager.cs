﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AionChatlogParser.EventListeners
{
    public class EntityManager
    {
        private ConcurrentDictionary<string, Entity> m_dic = new ConcurrentDictionary<string, Entity>();

        public event Action<Entity> EntityAdded;

        public Entity GetEntity(string str)
        {
            Entity e;

            if (!m_dic.TryGetValue(str, out e))
            {
                e = new Entity(str);
                if (m_dic.TryAdd(str, e))
                {
                    //added new entity
                    EntityAdded?.Invoke(e);
                }
                else
                {
                    //somebody else added in the meantime
                    m_dic.TryGetValue(str, out e);
                }
            }
            else
            {//found value
            }

            return m_dic.GetOrAdd(str, (s) => new Entity(s));
        }

        public IList<Entity> GetAllEntities()
        {
            return m_dic.Values.ToArray();
        }
    }
}
