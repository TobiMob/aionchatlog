﻿using AionChatlogParser.EventListeners;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace AionChatlogParser
{
    public class EventListenerManager
    {
        #region Member
        private DateTime m_dtFrom = DateTime.MinValue;
        private DateTime m_dtTo = DateTime.MaxValue;

        private List<IEventListener> m_lListeners = new List<IEventListener>();
        private List<EventSubscription> m_lSubscriptions = new List<EventSubscription>();

        private Thread m_tWorker = null;
        private string m_strFilePath = "";
        private LogReader.Mode m_readerMode = LogReader.Mode.Continuously;
        private int m_nReadInterval = 1000;
        private LogReader m_currentReader = null;

        private EntityManager m_entityManager = new EntityManager();
        #endregion

        #region Properties

        public EntityManager EntityManager
        {
            get { return m_entityManager; }
        }

        /// <summary>
        /// Current status of the reader.
        /// If null, everything is fine
        /// </summary>
        public Exception CurrentStatus { get; private set; }

        /// <summary>
        /// The Intervall in which the Chat.log file is checked
        /// </summary>
        public int ReadInterval
        {
            get { return m_nReadInterval; }
            set
            {
                m_nReadInterval = value;
                if (m_currentReader != null)
                    m_currentReader.ReadInterval = value;
            }
        }
        #endregion

        #region StartReading
        public void StartReading(string file, LogReader.Mode mode = LogReader.Mode.Continuously)
        {
            if (m_tWorker == null)
            {
                m_tWorker = new Thread(Worker);
                m_tWorker.IsBackground = true;
                m_tWorker.Start();
            }
            else
            {
            }

            m_strFilePath = file;
            m_readerMode = mode;
        }
        #endregion

        #region Worker
        /// <summary>
        /// Worker Thread
        /// </summary>
        private void Worker()
        {
            while (true)
            {
                string strStart = m_strFilePath;
                CurrentStatus = null;

                try
                {
                    using (LogReader reader = new LogReader())
                    {
                        m_currentReader = reader;
                        reader.ReadInterval = ReadInterval;

                        IEnumerable <string> workSource = reader.ReadLines(m_strFilePath, m_readerMode);

                        foreach (string str in workSource)
                        {
                            if (strStart != m_strFilePath)
                                break;

                            try
                            {
                                CheckLine(str);
                            }
                            catch (Exception e)
                            {
#if DEBUG
                                Debugger.Break();
#endif
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CurrentStatus = ex;
                    Thread.Sleep(2000);
                }
            }
        }
        #endregion

        #region Subscribe
        public void Subscribe(params IEventListener[] listeners)
        {
            Subscribe((IEnumerable<IEventListener>)listeners);
        }

        public void Subscribe(IEnumerable<IEventListener> listeners)
        {
            foreach (IEventListener listener in listeners)
            {
                m_lListeners.Add(listener);
                m_lSubscriptions.AddRange(listener.GetEvents());
                listener.Initialize(new EventListenerInitializeArgs() { EntityManager = m_entityManager });
            }
        }
        #endregion

        #region SetDtRange
        /// <summary>
        /// Specify if only the given time-range should be parsed
        /// </summary>
        public void SetDtRange(DateTime from, DateTime to)
        {
            m_dtFrom = from;
            m_dtTo = to;
        }
        #endregion

        #region DebugLine
        /// <summary>
        /// Checks the given line for events.
        /// Usable for debugging
        /// </summary>
        public void CheckLineDebug(string strText)
        {
            CheckLine(DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss") + " : " + strText);
        }
        #endregion

        #region CheckLine
        /// <summary>
        /// Checks the given line for events
        /// </summary>
        [DebuggerStepThrough]
        public void CheckLine(string strLine)
        {
            if (strLine.Length > 22)
            {
                string strTime = strLine.Substring(0, 19);
                string strText = strLine.Substring(22);

                //Parse Time
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(strTime, out dt);

                if (dt > m_dtFrom && dt < m_dtTo)
                {
                    CheckEvents(strText, dt);
                }
                else
                {//Ignore range
                }
            }
            else
            {//komischer Satz
            }
        }

        /// <summary>
        /// Checks the given line for events
        /// </summary>
        [DebuggerStepThrough]
        private void CheckEvents(string strText, DateTime dt)
        {
            bool blnFound = false;
            Regex rxLast = null;

            foreach (EventSubscription sub in m_lSubscriptions)
            {
                //Regexe dieser Subscription pruefen
                for (int i = 0; i < sub.Regexes.Count; i++)
                {
                    Regex r = sub.Regexes[i];
                    Match m = r.Match(strText);

                    if (m.Success)
                    {
                        LogEventArgs args = new LogEventArgs();
                        args.RegExMatch = m;
                        args.Timestamp = dt;

                        sub.Delegate(args);

                        if (blnFound)
                        {//duplicate
                        }

                        blnFound = true;
                        rxLast = r;
                    }
                    else
                    {//nope, naechster
                    }
                }
            }

            if (!blnFound)
            {
                //unknown
                //string s = strText;
            }
        }
        #endregion

        #region GetListener
        /// <summary>
        /// Returns a listener of the given type, if one exists.
        /// Otherwise return null
        /// </summary>
        public T GetListener<T>() where T : class, IEventListener
        {
            return m_lListeners.FirstOrDefault(x => x is T) as T;
        }
        #endregion
    }
}
