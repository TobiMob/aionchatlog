﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AionChatlogParser.EventListeners
{
    public abstract class EventListenerBase : IEventListener
    {
        public abstract string DisplayName
        {
            get;
        }

        protected EntityManager EntityManager
        {
            get; private set;
        }

        public abstract IEnumerable<EventSubscription> GetEvents();

        public void Initialize(EventListenerInitializeArgs args)
        {
            EntityManager = args.EntityManager;
        }
    }
}
