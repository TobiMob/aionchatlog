﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AionChatlogParser.EventListeners
{
    public class OtherListener : EventListenerBase
    {
        public override string DisplayName
        {
            get { return nameof(OtherListener); }
        }

        public override IEnumerable<EventSubscription> GetEvents()
        {
            yield return new EventSubscription($@"Ihr habt den Verbindungsstatus zu Online geändert\.", GenericEvent);
            yield return new EventSubscription($@"Möglicherweise könnt Ihr bestimmte Fertigkeiten oder Gegenstände in diesem Bereich nicht verwenden\.", GenericEvent);
            yield return new EventSubscription($@"Eine Umfrage ist eingetroffen. Klickt auf das Symbol, um das Umfragefenster zu öffnen\.", GenericEvent);
            yield return new EventSubscription($@"Ihr könnt von der feindlichen Fraktion angegriffen werden\.", GenericEvent);
            yield return new EventSubscription($@"Ihr seid dem (lokalen Kanal)|(Handelskanal)|(Kanal) (?<{g_name}>.+) beigetreten\.", GenericEvent);
            yield return new EventSubscription($@"\[.+?\] \[.+?\]: .+", GenericEvent);
            yield return new EventSubscription($@"^\[charname:(?<name>.+?);.+?\]:.+", GenericEvent);
            yield return new EventSubscription($@"Ihr könnt keine Quest erhalten, an der Ihr bereits arbeitet\.", GenericEvent);
            yield return new EventSubscription($@"Ihr könnt die (tägliche)|(wöchentliche) Quest annehmen\.", GenericEvent);
            yield return new EventSubscription($@"Ihr könnt die Fertigkeit nicht einsetzen, da die Bedingungen nicht erfüllt sind\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) ist betäubt, weil (?<name>.+?) (?<skill>.+) eingesetzt hat\.", GenericEvent);
            yield return new EventSubscription($@"Ungültiges Ziel\.", GenericEvent);

            yield return new EventSubscription($@"(?<target>.+?) weicht (?<skill>.+?) von (?<name>.+?) aus\.", GenericEvent);

            //yield return new EventSubscription($@"(?<name>.+?) weicht dem Angriff von (?<target>.+?) aus\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) blockt den Angriff von (?<target>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) pariert den Angriff von (?<target>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"Euer Schutzschild hält den Angriff von (?<name>.+?) auf\.", GenericEvent);
            yield return new EventSubscription($@"Der Angriff wird vom Schutzschild um (?<name>.+?) aufgehalten\.", GenericEvent);
            yield return new EventSubscription($@"Der Schutzschild um (?<target>.+?) hält den Angriff von (?<name>.+?) auf\.", GenericEvent);

            yield return new EventSubscription($@"Durch (?<skill>.+?) wurde Euer Attribut (?<attribute>.+?) erhöht\.", GenericEvent);
            yield return new EventSubscription($@"Die (?<attribute>.+?) von (?<target>.+?) werden durch (?<skill>.+?) von (?<name>.+?) gesenkt\.", GenericEvent);
            yield return new EventSubscription($@"Die (?<attribute>.+?) von (?<target>.+?) werden durch (?<skill>.+?) um (?<amount>.+?) gesenkt\.", GenericEvent);
            yield return new EventSubscription($@"Ihr habt (?<ep>.+?) EP von (?<name>.+?) erhalten.", GenericEvent);
            yield return new EventSubscription($@"Ihr habt (?<amount>\S+) Kinah erhalten\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) hat \[item:(\d+)\] erhalten\.", GenericEvent);
            yield return new EventSubscription($@"Ihr habt kein geeignetes Ziel für diese Fertigkeit\.", GenericEvent);
            yield return new EventSubscription($@"Ein dimensionaler Korridor, der zur (?<name>.+?) führt, ist erschienen\.", GenericEvent);

            yield return new EventSubscription($@"(?<name>.+?) (erhaltet|erhält) durch (?<skill>.+?) den Effekt '(?<effekt>.+?)'\.", GenericEvent);
            yield return new EventSubscription($@"^(?<target>.+?) hat (?<effekt>.+?), weil (?<name>\S+) (?<skill>.+?) ((benutzt)|(eingesetzt)) ((hat)|(habt))\.\s+$", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) hat ((Euer)|(sein)) Attribut (?<attribute>.+?) durch Benutzung von (?<skill>.+?) geändert\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) hat (?<attriute>.+?) von (?<target>.+?) durch Benutzung von (?<skill>.+?) geändert\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) hat den eigenen (?<attriute>.+?) durch (?<skill>.+?) geändert\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) hat eigene (?<attriute>.+?) durch (?<skill>.+?) aufgelöst\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) hat durch Benutzung von (?<skill>.+?) eine geänderte (?<attribute>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) hat Euer Attribut (?<attribute>.+?) durch (?<skill>.+?) erhöht\.", GenericEvent);

            yield return new EventSubscription($@"(?<skill>.+?) von (?<name>.+?) verträgt sich nicht mit der bestehenden Fertigkeit von (?<target>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) ist immun gegen (?<skill>.+?) von (?<name>.+?)\.", GenericEvent);

            yield return new EventSubscription($@"Ihr habt (?<target>.+?) durch (?<skill>.+?) betäubt\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) ist (?<status>.+?), weil (?<name>\S+) (?<skill>.+?) eingesetzt hat\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) erhält (?<damage>\S+) Giftschaden durch den Einsatz von Vergiftungseffekt (?<skill>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) ist zum Schweigen gebracht, weil (?<name>\S+) (?<skill>.+?) eingesetzt hat\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) ((seid)|(ist)) nicht mehr (?<effect>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>) dreht sich, weil (?<name>\S+) (?<skill>.+?) eingesetzt hat\.", GenericEvent);
            //yield return new EventSubscription($@"(?<target>.+?) ist nicht mehr vergiftet\.", GenericEvent);
            //yield return new EventSubscription($@"(?<target>.+?) ist nicht mehr betäubt\.", GenericEvent);
            //yield return new EventSubscription($@"(?<target>.+?) seit nicht mehr betäubt\.", GenericEvent);

            yield return new EventSubscription($@"(?<target>.+?) hat (?<effect>.+?) durch (?<skill>.+?) entfernt\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) hat (?<effect>.+?) von (?<target>.+?) durch (?<skill>.+?) aufgelöst\.", GenericEvent);
            yield return new EventSubscription($@"Eure (?<effect>.+?) wurden (entfernt|aufgelöst), weil (?<name>\S+) (?<skill>.+?) bei Euch eingesetzt hat. ", GenericEvent);


            yield return new EventSubscription($@"(Kritischer Treffer!)?(?<target>.+?) kann nicht fliegen, weil (?<name>\S+) (?<skill>.+?) hat\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) hat (durch (?<skill>.+?) )?(?<amount>\S+) (?<type>MP|TP) wiederhergestellt\.", GenericEvent);
            yield return new EventSubscription($@"Die (?<type>MP|TP) von (?<target>.+?) werden durch (Benutzung|das Wirken) von (?<skill>.+?) (fortwährend|kontinuierlich) wiederhergestellt\.", GenericEvent);
            yield return new EventSubscription($@"Die (?<type>MP|TP) von (?<target>.+?) werden fortwährend wiederhergestellt, weil (?<name>\S+) (?<skill>.+?) eingesetzt hat\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) stellt Eure (?<type>MP|TP) durch (?<target>.+?) fortwährend wieder her\.", GenericEvent);

            yield return new EventSubscription($@"(?<target>.+?) wurde durch einen Schock zurückgeworfen, weil (?<name>.+?) (?<skill>.+) ((eingesetzt hat)|(benutzt habt))\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) taumelt nicht mehr\.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) hat wieder die normale (?<attribute>.+?)\.", GenericEvent);
            yield return new EventSubscription($@"Ihr habt den Fertigkeitseinsatz des Ziels unterbrochen.", GenericEvent);
            yield return new EventSubscription($@"(?<target>.+?) widersteht (?<skill>.+) von (?<name>.+?)\.", GenericEvent);


            yield return new EventSubscription($@"Ihr habt den Beitritt zur Allianz von (?<target>.+?) beantragt\.", GenericEvent);
            yield return new EventSubscription($@"Die Beuteverteilung der Allianz wurde zu (?<loot>.+?), geändert.", GenericEvent);

            yield return new EventSubscription($@"(?<instance>.+?) mit einem Spielermaximum von (?<players>.+?) wurde geöffnet\.", GenericEvent);
            yield return new EventSubscription($@"Der Eintritt war erfolgreich. Die Anzahl der möglichen Eintritte wurde um eins verringert\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) ist selbst von (?<skill>.+?) betroffen\.", GenericEvent);

            yield return new EventSubscription($@"(?<name>.+?) ist gestorben\.", GenericEvent);
            yield return new EventSubscription($@"Hyperions Schutzschild wird schwächer\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) ist dem Äthergriff entkommen\.", GenericEvent);
            yield return new EventSubscription($@"(?<name>.+?) blutet, weil (?<name>.+?) (?<skill>.+) eingesetzt hat\.", GenericEvent);
        }

        private void GenericEvent(LogEventArgs args)
        {
        }

        private const string g_name = "name";

        private readonly string C_JoinedAlliDE = $@"(?<{g_name}>.+) (ist|seid) der Allianz beigetreten\.";
    }
}
