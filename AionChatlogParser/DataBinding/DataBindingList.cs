﻿using AionChatlogParser.EventListeners;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AionChatlogParser
{
    [Flags]
    public enum DataInvalidateion
    {
        None = 0,
        Value,
        Filter
    }

    public interface IDataEntry
    {
        string Name { get; }
        float Value { get; }
    }

    public class DataBindingList<TEntity, TSubData> : DataTable
        where TEntity : INotifyPropertyChanged
    {
        #region DataEntry
        class DataEntry : INotifyPropertyChanged, IDataEntry
        {
            internal TEntity Entity { get; set; }
            private Func<TEntity, string> NameFunction = null;
            private Func<TEntity, float> ValueFunction = null;
            private Action<DataEntry, TEntity, TSubData, PropertyChangedEventArgs> DataPropertyChanged = null;

            public DataRow Row { get; set; }

            public bool IsVisible => (Row != null);
            
            public string Name => NameFunction(Entity);
            public float Value => ValueFunction(Entity);

            public DataEntry(TEntity e, Func<TEntity, string> GetName, Func<TEntity, float> GetValue, Action<DataEntry, TEntity, TSubData, PropertyChangedEventArgs> dataPropertyChanged)
            {
                Entity = e;
                NameFunction = GetName;
                ValueFunction = GetValue;
                DataPropertyChanged = dataPropertyChanged;
                e.PropertyChanged += Data_PropertyChanged;
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void Data_PropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                DataPropertyChanged?.Invoke(this, Entity, (TSubData)sender, e);
            }

            public void CallPropertyChanged()
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
            }
        }
        #endregion

        #region Member
        private EventListenerManager m_manager = null;
        private List<DataEntry> m_lAllEntries = new List<DataEntry>();

        /// <summary>
        /// Used to invoke the UI-Thread
        /// </summary>
        private AsyncOperation m_uiContext = null;
        #endregion

        #region Properties
        /// <summary>
        /// Function to get the value of a <see cref="TEntity"/>
        /// </summary>
        public Func<TEntity, float> FunctionGetValue { get; set; }

        /// <summary>
        /// Function to get the name of a <see cref="TEntity"/>
        /// </summary>
        public Func<TEntity, string> FunctionGetName { get; set; }

        /// <summary>
        /// Funtion to check if the <see cref="TEntity"/> should be visible
        /// </summary>
        public Func<TEntity, bool> FunctionFilter { get; set; }

        /// <summary>
        /// Function which is called, if a entry changes.
        /// Returns the <see cref="DataInvalidateion"/> to tell the datasource, if the <see cref="DataEntry"/> needs a update
        /// </summary>
        public Func<TEntity, TSubData, PropertyChangedEventArgs, DataInvalidateion> FunctionEntryChanged { get; set; }
        #endregion

        #region ctor
        public DataBindingList(EventListenerManager manager)
        {
            m_manager = manager;

            //Save UI Context for later
            m_uiContext = AsyncOperationManager.CreateOperation(null);

            Columns.Add(new DataColumn("Name"));
            Columns.Add(new DataColumn("Value", typeof(float)));
        }
        #endregion

        #region Add
        /// <summary>
        /// Add a entry to the list.
        /// Does nothing, if the entry is already in the list
        /// </summary>
        /// <param name="entity"></param>
        public void Add(TEntity entity)
        {
            if (!Contains(entity))
            {
                DataEntry entry = null;
                lock (m_lAllEntries)
                {
                    if (!Contains(entity))
                    {
                        entry = new DataEntry(entity, NameFunction, ValueFunction, Entry_PropertyChanged);
                        m_lAllEntries.Add(entry);
                    }
                }

                if(entry != null)
                    CheckForVisibility(entry);
            }
            else
            {//already there
            }
        }
        #endregion

        #region Contains
        /// <summary>
        /// Checks if the entry is in the list
        /// </summary>
        public bool Contains(TEntity value)
        {
            Entity e = value as Entity;

            if (e == null)
                return false;

            foreach (DataEntry entry in m_lAllEntries)
            {
                if (entry.Name == e.Name)
                    return true;
            }

            return false;
        }

        public void RecheckFilter()
        {
            foreach (DataEntry entry in m_lAllEntries)
            {
                CheckForVisibility(entry);
            }
        }
        #endregion

        #region CheckForVisibility
        /// <summary>
        /// Check if the visibility of the entry changed
        /// </summary>
        private void CheckForVisibility(DataEntry entry)
        {
            bool blnShouldBeVisible = FunctionFilter(entry.Entity);

            if (blnShouldBeVisible && !entry.IsVisible)
            {
                //Entry has changed to visible
                m_uiContext.Post((arg) =>
                {
                    if (entry.IsVisible)
                        return;

                    DataRow row = NewRow();
                    entry.Row = row;

                    row[0] = entry.Name;
                    row[1] = entry.Value;

                    Rows.Add(row);
                }, null);
            }
            else if (!blnShouldBeVisible && entry.IsVisible)
            {
                //Entry has changed to not visible
                m_uiContext.Post((arg) =>
                {
                    if (entry.IsVisible)
                    {
                        Rows.Remove(entry.Row);
                        entry.Row = null;
                    }
                }, null);
            }
            else
            {//Visibility did not change
            }
        }
        #endregion

        #region Entry_ProeprtyChanged
        /// <summary>
        /// Called if the Item of a <see cref="DataEntry"/> changed
        /// </summary>
        private void Entry_PropertyChanged(DataEntry entry, TEntity entity, TSubData subData, PropertyChangedEventArgs args)
        {
            DataInvalidateion invalidation;

            if (FunctionEntryChanged != null)
            {
                invalidation = FunctionEntryChanged(entity, subData, args);
            }
            else
            {
                invalidation = DataInvalidateion.None;
            }

            if (invalidation.HasFlag(DataInvalidateion.Value))
            {
                //Update Value
                if (entry.IsVisible)
                {
                    m_uiContext.Post((arg) =>
                    {
                        if (entry.IsVisible)
                        {
                            entry.Row[1] = entry.Value;
                        }
                        else
                        {//no longer visible
                        }
                    }
                    , null);
                }
                else
                {//not shown
                }
            }

            if (invalidation.HasFlag(DataInvalidateion.Filter))
            {
                //Update Visibility
                CheckForVisibility(entry);
            }
        }
        #endregion

        #region NameFunction
        private string NameFunction(TEntity entity)
        {
            if (FunctionGetName != null)
                return FunctionGetName(entity);
            else
                return "";

        }
        #endregion

        #region ValueFunction
        private float ValueFunction(TEntity entity)
        {
            if (FunctionGetValue != null)
                return FunctionGetValue(entity);
            else
                return 0;

        }
        #endregion

    }
}
