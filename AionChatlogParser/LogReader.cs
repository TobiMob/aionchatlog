﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AionChatlogParser
{
    public class LogReader : IDisposable
    {
        public enum Mode
        {
            ToEnd,
            Continuously,
        }

        public long Position { get; private set; }
        public long Size { get; private set; }

        public int ReadInterval = 1000;
        public bool Running { get; private set; }

        public LogReader()
        {

        }

        public IEnumerable<string> ReadLines(string strPath, Mode mode = Mode.ToEnd)
        {
            Running = true;

            using (FileStream stream = new FileStream(strPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                //goto file end
                if (mode == Mode.Continuously)
                {
                    if(stream.Length > 1025)
                        stream.Seek(-1024, SeekOrigin.End);
                }

                using (StreamReader reader = new StreamReader(stream, Encoding.Default))
                {
                    do
                    {
                        while (!reader.EndOfStream && Running)
                        {
                            yield return reader.ReadLine();
                            Size = reader.BaseStream.Length;
                            Position = reader.BaseStream.Position;
                        }

                        if (!Running)
                            break;

                        if (mode == Mode.Continuously)
                            Thread.Sleep(ReadInterval);
                    }
                    while (mode == Mode.Continuously);
                }
            }
        }

        public void Dispose()
        {
            Running = false;
        }
    }
}
