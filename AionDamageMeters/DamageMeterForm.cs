﻿using AionChatlogParser;
using AionChatlogParser.EventListeners;
using AionDamageMeters.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AionDamageMeters
{
    public partial class DamageMeterForm : Form
    {
        #region Member
        private DamageListener m_listener = new DamageListener();
        private FactionListener m_factionListener = new FactionListener();
        private EventListenerManager m_eventManager = new EventListenerManager();
        private DataBindingList<Entity, Data> m_dataSource = null;
        #endregion

        #region Ctor
        /// <summary>
        /// Ctor
        /// </summary>
        public DamageMeterForm()
        {
            Settings.Default.Reload();

            InitializeComponent();
            m_eventManager.Subscribe(m_listener);
            m_eventManager.Subscribe(m_factionListener);
            m_eventManager.Subscribe(new OtherListener());

            m_eventManager.EntityManager.EntityAdded += EntityManager_EntityAdded;

            m_grid.DataSourceChanged += M_grid_DataSourceChanged;

            m_dataSource = new DataBindingList<Entity, Data>(m_eventManager);
            m_grid.DataSource = m_dataSource;
            
            m_dataSource.FunctionFilter = FilterFunction;
            m_dataSource.FunctionGetName = NameFunction;
            m_dataSource.FunctionGetValue = ValueFunction;
            m_dataSource.FunctionEntryChanged = EntryChanged;

#if DEBUG
            m_eventManager.StartReading(Settings.Default.ChatLogPath, LogReader.Mode.ToEnd);
            new DebugForm(m_eventManager).Show();
#else
            m_eventManager.StartReading(Settings.Default.ChatLogPath);
#endif
        }

        private void EntityManager_EntityAdded(Entity obj)
        {
            m_dataSource.Add(obj);
        }

        private DataInvalidateion EntryChanged(Entity arg1, Data arg2, PropertyChangedEventArgs arg3)
        {
            return DataInvalidateion.Filter | DataInvalidateion.Value;
        }

        private float ValueFunction(Entity arg)
        {
            return arg.GetData<DamageData>().DamageDealt;
        }

        private string NameFunction(Entity arg)
        {
            return arg.Name;
        }

        private bool FilterFunction(Entity arg)
        {
            Faction f = arg.GetData<FactionData>().Faction;
            float d = arg.GetData<DamageData>().DamageDealt;

            return (d > 0) &&
                   (f.HasFlag(m_faction));
        }
        #endregion

        #region M_grid_DataSourceChanged
        /// <summary>
        /// Setup datagrid for its datasource
        /// </summary>
        private void M_grid_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView grid = (DataGridView)sender;

            //Set default sort for Damage Column
            foreach (DataGridViewColumn column in grid.Columns)
            {
                if (column.Name == "Value")
                {
                    GuiUtils.ToNumberColumn(column);
                    break;
                }
            }
        }
        #endregion

        private void m_grid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            m_context_grid.Show(Cursor.Position);
        }

        #region toolStrip1_MouseDown
        private void toolStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GuiUtils.StartFormDrag(Handle);
            }
        }
        #endregion


        private void m_tsClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void m_tsSettings_Click(object sender, EventArgs e)
        {
            SettingsDialog frm = new SettingsDialog(m_eventManager);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog(this);
        }

        private void m_tsDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(this, "Schaden auf 0 zurücksetzen?", "Schaden zurücksetzen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
                m_listener.ResetDamage();
        }

        private void m_ts_zurPartyHinzufuegen_Click(object sender, EventArgs e)
        {
            Entity entity = GetSelectedEntity();
            if (entity != null)
            {
                entity.GetData<FactionData>().Faction = Faction.Ally;
                m_dataSource.RecheckFilter();
            }
        }

        private void m_ts_ausPartyEntfernenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Entity entity = GetSelectedEntity();
            if (entity != null)
            {
                entity.GetData<FactionData>().Faction = Faction.Any;
                m_dataSource.RecheckFilter();
            }

        }

        private Faction m_faction = Faction.Any;

        private void m_tsSelectAll_Click(object sender, EventArgs e)
        {
            if (m_faction != Faction.Any)
            {
                m_faction = Faction.Any;
                m_dataSource.RecheckFilter();

                m_tsEntitySelection.Text = m_tsSelectAll.Text;
            }
        }

        private void m_tsSelectParty_Click(object sender, EventArgs e)
        {
            if (m_faction != Faction.Ally)
            {
                m_faction = Faction.Ally;
                m_dataSource.RecheckFilter();

                m_tsEntitySelection.Text = m_tsSelectParty.Text;
            }
        }

        private void m_tsPin_Click(object sender, EventArgs e)
        {
            if (TopMost)
            {
                TopMost = false;
                m_tsPin.Image = Resources.Pin;
            }
            else
            {
                TopMost = true;
                m_tsPin.Image = Resources.Pin2;
            }
        }

        private void detailsAnzeigenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Entity entity = GetSelectedEntity();
            if (entity != null)
            {
                DamageData data = entity.GetData<DamageData>();
                DamageDetails form = new DamageDetails(data);
                form.StartPosition = FormStartPosition.CenterParent;
                form.Show(this);
            }
        }

        private Entity GetSelectedEntity()
        {
            if (m_grid.SelectedRows.Count > 0)
            {
                string entity = m_grid.SelectedRows[0].Cells[0].Value.ToString();
                return m_eventManager.EntityManager.GetEntity(entity);
            }
            else
            {
                return null;
            }
        }
    }
}
