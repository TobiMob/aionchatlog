﻿namespace AionDamageMeters
{
    partial class DamageMeterForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DamageMeterForm));
            this.m_statusTimer = new System.Windows.Forms.Timer(this.components);
            this.m_context_grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_ts_zurPartyHinzufuegen = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ts_ausPartyEntfernenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_grid = new System.Windows.Forms.DataGridView();
            this.m_tsDamageType = new System.Windows.Forms.ToolStripDropDownButton();
            this.m_tsClose = new System.Windows.Forms.ToolStripButton();
            this.m_tsSettings = new System.Windows.Forms.ToolStripButton();
            this.m_tsEntitySelection = new System.Windows.Forms.ToolStripDropDownButton();
            this.m_tsSelectParty = new System.Windows.Forms.ToolStripMenuItem();
            this.m_tsSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.m_tsDelete = new System.Windows.Forms.ToolStripButton();
            this.m_tsPin = new System.Windows.Forms.ToolStripButton();
            this.detailsAnzeigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_context_grid.SuspendLayout();
            this.m_toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // m_statusTimer
            // 
            this.m_statusTimer.Enabled = true;
            this.m_statusTimer.Interval = 1000;
            // 
            // m_context_grid
            // 
            this.m_context_grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detailsAnzeigenToolStripMenuItem,
            this.m_ts_zurPartyHinzufuegen,
            this.m_ts_ausPartyEntfernenToolStripMenuItem});
            this.m_context_grid.Name = "m_context_grid";
            this.m_context_grid.Size = new System.Drawing.Size(184, 92);
            // 
            // m_ts_zurPartyHinzufuegen
            // 
            this.m_ts_zurPartyHinzufuegen.Name = "m_ts_zurPartyHinzufuegen";
            this.m_ts_zurPartyHinzufuegen.Size = new System.Drawing.Size(192, 22);
            this.m_ts_zurPartyHinzufuegen.Text = "zur Party hinzufügen";
            this.m_ts_zurPartyHinzufuegen.Click += new System.EventHandler(this.m_ts_zurPartyHinzufuegen_Click);
            // 
            // m_ts_ausPartyEntfernenToolStripMenuItem
            // 
            this.m_ts_ausPartyEntfernenToolStripMenuItem.Name = "m_ts_ausPartyEntfernenToolStripMenuItem";
            this.m_ts_ausPartyEntfernenToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.m_ts_ausPartyEntfernenToolStripMenuItem.Text = "aus Party entfernen";
            this.m_ts_ausPartyEntfernenToolStripMenuItem.Click += new System.EventHandler(this.m_ts_ausPartyEntfernenToolStripMenuItem_Click);
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.AutoSize = false;
            this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_tsDamageType,
            this.m_tsClose,
            this.m_tsSettings,
            this.m_tsEntitySelection,
            this.toolStripSeparator1,
            this.m_tsDelete,
            this.m_tsPin});
            this.m_toolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.Size = new System.Drawing.Size(284, 26);
            this.m_toolStrip.TabIndex = 5;
            this.m_toolStrip.Text = "toolStrip1";
            this.m_toolStrip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStrip1_MouseDown);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 26);
            // 
            // m_grid
            // 
            this.m_grid.AllowUserToAddRows = false;
            this.m_grid.AllowUserToDeleteRows = false;
            this.m_grid.AllowUserToResizeRows = false;
            this.m_grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_grid.Location = new System.Drawing.Point(0, 26);
            this.m_grid.Name = "m_grid";
            this.m_grid.ReadOnly = true;
            this.m_grid.RowHeadersVisible = false;
            this.m_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_grid.Size = new System.Drawing.Size(284, 274);
            this.m_grid.TabIndex = 6;
            this.m_grid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_grid_CellClick);
            // 
            // m_tsDamageType
            // 
            this.m_tsDamageType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.m_tsDamageType.Image = ((System.Drawing.Image)(resources.GetObject("m_tsDamageType.Image")));
            this.m_tsDamageType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsDamageType.Name = "m_tsDamageType";
            this.m_tsDamageType.Size = new System.Drawing.Size(65, 23);
            this.m_tsDamageType.Text = "Schaden";
            this.m_tsDamageType.ToolTipText = "Daten auswahl";
            // 
            // m_tsClose
            // 
            this.m_tsClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_tsClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsClose.Image = global::AionDamageMeters.Properties.Resources.Exit;
            this.m_tsClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsClose.Name = "m_tsClose";
            this.m_tsClose.Size = new System.Drawing.Size(23, 23);
            this.m_tsClose.Text = "Beenden";
            this.m_tsClose.Click += new System.EventHandler(this.m_tsClose_Click);
            // 
            // m_tsSettings
            // 
            this.m_tsSettings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_tsSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsSettings.Image = global::AionDamageMeters.Properties.Resources.Settings;
            this.m_tsSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsSettings.Name = "m_tsSettings";
            this.m_tsSettings.Size = new System.Drawing.Size(23, 23);
            this.m_tsSettings.Text = "Einstellungen";
            this.m_tsSettings.Click += new System.EventHandler(this.m_tsSettings_Click);
            // 
            // m_tsEntitySelection
            // 
            this.m_tsEntitySelection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.m_tsEntitySelection.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_tsSelectParty,
            this.m_tsSelectAll});
            this.m_tsEntitySelection.Image = ((System.Drawing.Image)(resources.GetObject("m_tsEntitySelection.Image")));
            this.m_tsEntitySelection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsEntitySelection.Name = "m_tsEntitySelection";
            this.m_tsEntitySelection.Size = new System.Drawing.Size(40, 23);
            this.m_tsEntitySelection.Text = "Alle";
            this.m_tsEntitySelection.ToolTipText = "Einheiten auswahl";
            // 
            // m_tsSelectParty
            // 
            this.m_tsSelectParty.Name = "m_tsSelectParty";
            this.m_tsSelectParty.Size = new System.Drawing.Size(141, 22);
            this.m_tsSelectParty.Text = "Allianz/Party";
            this.m_tsSelectParty.Click += new System.EventHandler(this.m_tsSelectParty_Click);
            // 
            // m_tsSelectAll
            // 
            this.m_tsSelectAll.Name = "m_tsSelectAll";
            this.m_tsSelectAll.Size = new System.Drawing.Size(141, 22);
            this.m_tsSelectAll.Text = "Alle";
            this.m_tsSelectAll.Click += new System.EventHandler(this.m_tsSelectAll_Click);
            // 
            // m_tsDelete
            // 
            this.m_tsDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_tsDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsDelete.Image = global::AionDamageMeters.Properties.Resources.Delete;
            this.m_tsDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsDelete.Name = "m_tsDelete";
            this.m_tsDelete.Size = new System.Drawing.Size(23, 23);
            this.m_tsDelete.Text = "Einträge löschen";
            this.m_tsDelete.Click += new System.EventHandler(this.m_tsDelete_Click);
            // 
            // m_tsPin
            // 
            this.m_tsPin.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_tsPin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsPin.Image = global::AionDamageMeters.Properties.Resources.Pin;
            this.m_tsPin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsPin.Name = "m_tsPin";
            this.m_tsPin.Size = new System.Drawing.Size(23, 23);
            this.m_tsPin.Text = "Im Vordergrund bleiben";
            this.m_tsPin.Click += new System.EventHandler(this.m_tsPin_Click);
            // 
            // detailsAnzeigenToolStripMenuItem
            // 
            this.detailsAnzeigenToolStripMenuItem.Name = "detailsAnzeigenToolStripMenuItem";
            this.detailsAnzeigenToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.detailsAnzeigenToolStripMenuItem.Text = "Details anzeigen";
            this.detailsAnzeigenToolStripMenuItem.Click += new System.EventHandler(this.detailsAnzeigenToolStripMenuItem_Click);
            // 
            // DamageMeterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 300);
            this.ControlBox = false;
            this.Controls.Add(this.m_grid);
            this.Controls.Add(this.m_toolStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "DamageMeterForm";
            this.m_context_grid.ResumeLayout(false);
            this.m_toolStrip.ResumeLayout(false);
            this.m_toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer m_statusTimer;
        private System.Windows.Forms.ContextMenuStrip m_context_grid;
        private System.Windows.Forms.ToolStrip m_toolStrip;
        private System.Windows.Forms.ToolStripButton m_tsClose;
        private System.Windows.Forms.ToolStripButton m_tsSettings;
        private System.Windows.Forms.ToolStripDropDownButton m_tsDamageType;
        private System.Windows.Forms.DataGridView m_grid;
        private System.Windows.Forms.ToolStripDropDownButton m_tsEntitySelection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton m_tsDelete;
        private System.Windows.Forms.ToolStripMenuItem m_ts_zurPartyHinzufuegen;
        private System.Windows.Forms.ToolStripMenuItem m_ts_ausPartyEntfernenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_tsSelectParty;
        private System.Windows.Forms.ToolStripMenuItem m_tsSelectAll;
        private System.Windows.Forms.ToolStripButton m_tsPin;
        private System.Windows.Forms.ToolStripMenuItem detailsAnzeigenToolStripMenuItem;
    }
}

