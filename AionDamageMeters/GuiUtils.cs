﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace AionDamageMeters
{
    public static class GuiUtils
    {
        public static Point Add(this Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point Subtract(this Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }

        #region FormDrag
        /// <summary>
        /// Starts to drag the form, until the mouse-button is released
        /// </summary>
        /// <param name="formHandle"></param>
        public static void StartFormDrag(IntPtr formHandle)
        {
            ReleaseCapture();
            SendMessage(formHandle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        #region ToNumberColumn
        /// <summary>
        /// Setup datagrid for its datasource
        /// </summary>
        public static void ToNumberColumn(DataGridViewColumn column)
        {
            column.DataGridView.Sort(column, ListSortDirection.Descending);
            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            column.DefaultCellStyle.Format = "#,#";
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            column.DefaultCellStyle.Font = new Font("Courier New", SystemFonts.DefaultFont.Size);
        }
        #endregion
    }
}
