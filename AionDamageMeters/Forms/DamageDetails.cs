﻿using AionChatlogParser.EventListeners;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AionDamageMeters
{
    public partial class DamageDetails : Form
    {
        public DamageDetails(DamageData data)
        {
            InitializeComponent();

            UpdateGrid(data);

            data.PropertyChanged += Data_PropertyChanged;
            //data.OnPropertyChanged(null, null);
        }

        private void Data_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Invoke((Action)(() => UpdateGrid((DamageData)sender)));
        }

        private void UpdateGrid(DamageData data)
        {
            dataGridView1.SuspendLayout();
            dataGridView1.Rows.Clear();

            var entries = data.GetEntries();

            foreach (var pair in entries)
            {
                int n = dataGridView1.Rows.Add();
                dataGridView1.Rows[n].Cells[0].Value = pair.Key;
                dataGridView1.Rows[n].Cells[1].Value = pair.Value;
            }

            GuiUtils.ToNumberColumn(m_colValue);

            dataGridView1.ResumeLayout();
        }

        #region M_grid_DataSourceChanged
        /// <summary>
        /// Setup datagrid for its datasource
        /// </summary>
        private void M_grid_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView grid = (DataGridView)sender;

            //Set default sort for Damage Column
            foreach (DataGridViewColumn column in grid.Columns)
            {
                if (column.Name == "Value")
                {
                    GuiUtils.ToNumberColumn(column);
                    break;
                }
            }
        }
        #endregion
    }
}
