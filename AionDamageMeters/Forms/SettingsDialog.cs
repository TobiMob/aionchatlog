﻿using AionChatlogParser;
using AionDamageMeters.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AionDamageMeters
{
    public partial class SettingsDialog : Form
    {
        EventListenerManager m_eventManager = null;

        public SettingsDialog(EventListenerManager manager)
        {
            m_eventManager = manager;
            InitializeComponent();
        }

        private void m_btnChangePath_Click(object sender, EventArgs e)
        {
            OpenFileDialog frm = new OpenFileDialog();

            frm.Filter = "Chat.log|Chat.log";
            frm.InitialDirectory = new FileInfo(Settings.Default.ChatLogPath).Directory.FullName;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                Settings.Default.ChatLogPath = frm.FileName;
                Settings.Default.Save();

                m_eventManager.StartReading(Settings.Default.ChatLogPath);
            }
        }

        private void statusTimer_Tick(object sender, EventArgs e)
        {
            bool blnOk;
            string strText;

            if (m_eventManager != null)
            {
                if (m_eventManager.CurrentStatus != null)
                {
                    strText = m_eventManager.CurrentStatus.Message;
                    blnOk = false;
                }
                else
                {
                    strText = "OK";
                    blnOk = true;
                }
            }
            else
            {
                strText = "Reader noch nicht gestartet";
                blnOk = false;
            }

            m_txtReaderStatus.BackColor = (blnOk) ? Color.Green : Color.Red;
            m_txtReaderStatus.Text = strText;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            m_eventManager.ReadInterval = (int)numericUpDown1.Value;
        }
    }
}
