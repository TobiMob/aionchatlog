﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EnableChatlog
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                OpenFileDialog fileDialog = new OpenFileDialog();

                fileDialog.InitialDirectory = new FileInfo(@"C:\Program Files (x86)\Gameforge\AION Free-To-Play\Chat.log").Directory.FullName;
                fileDialog.Filter = "system.cfg|system.cfg";

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    FileInfo file = new FileInfo(fileDialog.FileName);

                    if (file.Exists)
                    {
                        FileInfo ovrFile = new FileInfo(Path.Combine(file.Directory.FullName, "system.ovr"));


                        if (!ovrFile.Exists)
                        {
                            ovrFile.Create().Dispose();
                        }

                        string strChatLogSetting = "g_chatlog = \"1\"";

                        string strOvrContent = File.ReadAllText(ovrFile.FullName);
                        bool blnAlreadyWritte = strOvrContent.Contains(strChatLogSetting);

                        if (!blnAlreadyWritte)
                        {
                            using (StreamWriter writer = File.AppendText(ovrFile.FullName))
                            {
                                if (!strOvrContent.EndsWith(writer.NewLine))
                                    writer.WriteLine();

                                writer.WriteLine(strChatLogSetting);

                                MessageBox.Show($"Der Eintrag <{strChatLogSetting}> wurde in die Datei geschrieben.\n\rAION muss neu gestartet werden, bevor die Änderungen aktiv werden.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            }
                        }
                        else
                        {
                            MessageBox.Show($"Der Eintrag <{strChatLogSetting}> ist in der Datei bereits vorhanden.\n\rDie Datei wurde nicht verändert.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                    else
                    {
                        throw new FileNotFoundException("Ausgewählte Datei existiert nicht", file.FullName);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
